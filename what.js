const fs = require('fs');

const folderName = 'C:\sd';

let d = fs.readFileSync("count.txt", "utf8");

const path = require("path");
fs.writeFile('count.txt', d + "5", (error) => {
	//fd - это дескриптор файла
});

//fs.mkdir(path.resolve('dasad'));

// Normalization
console.log('normalization : ' + path.normalize('/test/test1//2slashes/1slash/tab/..'));

// Join
console.log('joint path : ' + path.join('/test', 'test1', '2slashes/1slash', 'tab', '..'));

// Resolve
console.log('resolve : ' + path.resolve('dasad'));

// extName
console.log('ext name : ' + path.extname('what.js'));